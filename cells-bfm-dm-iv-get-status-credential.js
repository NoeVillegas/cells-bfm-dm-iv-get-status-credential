class CellsBfmDmIvGetStatusCredential extends Polymer.Element {

  static get is() {
    return 'cells-bfm-dm-iv-get-status-credential';
  }

  static get properties() {
    return {
      endpoint: String,
      headers: {
        type: Object,
        value: () => ({})
      },
      host: {
        type: String
      },
      id: {
        type: String
      },
      count:{
        type: Number,
        value: 0
      }
    };
  }
  /**
   * Function to handle service success response
   * @param {*} response
   */
  _successResponse(response) {
    this.count++;
    var has200 = response.detail.response.filter((status)=>{
      return status.code >= 200 && status.code <= 210;
    });
    //TODO when has 40* error handler event
    var has400 = response.detail.response.filter((status)=>{
      return  status.code >= 400;
    });


    if (has200.length) {
      this.dispatchEvent(new CustomEvent('success-add-credentials', {
        detail: {
          headIlustration: {
            type: 'icon',
            icon: 'coronita:correct',
          },
          title: 'Facturas agregadas con éxito',
          detail:'Ya puede revisar las facturas emitidas y recibidas de EMPRESA ALPHA SA de CV dentro de BBVA One View Facturación.',
        },
      }));
      setTimeout(() => {
        this.dispatchEvent(new CustomEvent('success-navigation-event'));
      }, 1000);
      this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
      this.dispatchEvent(new CustomEvent('all-credential-status', {detail: response.detail.response}));
    }else if(!has200.length && has400.length){
      this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
      this.dispatchEvent(new CustomEvent('error-add-credentials'));
    } else {
      if(this.count < 50){
        setTimeout(function () {
          this.$.dpStatusCredential.generateRequest();
        }.bind(this), 250);
      }else{
        this.dispatchEvent(new CustomEvent('success-add-credentials'));
      this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
      }
    }
    if(this.count > 40){
      let host = this._getHost();
      this.set('endpoint', `${host}/status_ok.json`);
    }

    // else {
    //   this.dispatchEvent(new CustomEvent('error-add-credentials'));
    // }
   // this.dispatchEvent(new CustomEvent('all-credential-status', {detail: response.detail.response}));
  }
  /**
   * Function to generate enpoint and call service
   */
  starterMethod(jobId) {
    this._buildEndpoint(jobId);
    if (this.endpoint && jobId) {
      this.$.dpStatusCredential.generateRequest();
    }
  }

  /**
   * function to buil enpoint url
   */
  _buildEndpoint(jobId) {
    let host = this._getHost();
    let isMock = this._getMocks();
    if(isMock){
      this.set('endpoint', `${host}/status_pending${isMock}`);
    }else{
      this.set('endpoint', `${host}/jobs/${jobId}/status`);
    }
  }

  _getMocks(){
    return (window.AppConfig && window.AppConfig.mocks) ? '.json' : '';
  }
  /**
   * Function to get host from config file
   */
  _getHost() {
    return (window.AppConfig && window.AppConfig.host) ? window.AppConfig.host : this.host;
  }
  /**
   * Function to hanlde error status response
   * @param {*} response
   */
  _handlerError(response) {
    var msj = 'Estamos teniendo dificultades técnicas, favor de intentar mas tarde';
    try {
      var errorCode = response.detail.code;
      switch (errorCode) {
        case 401:
          this.dispatchEvent(new CustomEvent('unauthorized-error', { detail: response.detail.code }));
          break;
        default:
          msj = response.detail['error-message'] || errorCode || msj;
          break;
      }
    } catch (e) {
      //No aplicable catch handler, use default values from eventError and msj
    }
    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    this.dispatchEvent(new CustomEvent('error-service', { detail: msj }));
    this.dispatchEvent(new CustomEvent('error-add-credentials'));
  }
}

customElements.define(CellsBfmDmIvGetStatusCredential.is, CellsBfmDmIvGetStatusCredential);
